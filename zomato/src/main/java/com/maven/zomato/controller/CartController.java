package com.maven.zomato.controller;

import com.maven.zomato.domain.Inventory;
import com.maven.zomato.domain.Menu;
import com.maven.zomato.repository.InventoryRepository;
import com.maven.zomato.repository.MenuRepository;
import com.maven.zomato.service.CartRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CartController {
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private MenuRepository menuRepository;

    @PostMapping("/addCart")
    public Inventory addCart(@RequestBody CartRequest request){
        return inventoryRepository.save(request.getInventory());
    }
    @GetMapping("/findAll")
    public List<Inventory>getAll(){
        return inventoryRepository.findAll();
    }

}
