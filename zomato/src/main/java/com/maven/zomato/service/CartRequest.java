package com.maven.zomato.service;

import com.maven.zomato.domain.Inventory;
import org.springframework.stereotype.Component;

@Component
public class CartRequest {
    private Inventory inventory;

    public CartRequest() {
    }

    public CartRequest(Inventory inventory) {
        this.inventory = inventory;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    @Override
    public String toString() {
        return "CartRequest{" +
                "inventory=" + inventory +
                '}';
    }
}
