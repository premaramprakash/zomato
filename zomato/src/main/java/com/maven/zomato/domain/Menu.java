package com.maven.zomato.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "menu_list")
public class Menu {
    @Id
    private int menuId;
    @Column
    private String menuItem;
    private long price;
    public Menu() {
    }

    public Menu(int menuId, String menuItem, long price) {
        this.menuId = menuId;
        this.menuItem = menuItem;
        this.price = price;
    }

    public long getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public String getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(String menuItem) {
        this.menuItem = menuItem;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "menuId=" + menuId +
                ", menuItem='" + menuItem + '\'' +
                ", price=" + price +
                '}';
    }
}
