package com.maven.zomato.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "inventory_list")
public class Inventory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    private String itemName;
    private long quantity;
    @OneToMany(targetEntity = Menu.class,cascade = CascadeType.ALL)
    @JoinColumn(name = "menuId",referencedColumnName = "id")
    private List<Menu>menuList;
    public Inventory() {
    }

    public Inventory(long id, String itemName, long quantity) {
        this.id = id;
        this.itemName = itemName;
        this.quantity = quantity;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenus(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "id=" + id +
                ", itemName='" + itemName + '\'' +
                ", quantity=" + quantity +
                ", menuList=" + menuList +
                '}';
    }
}
